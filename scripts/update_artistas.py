"""
Script para atualizar o arquivo static/artistas.json com os dados da planilha do Google.
Esse script é necessário para tornar mais simples para outras pessoas atualizarem a listagem de artistas
enquanto ainda não temos um backend que guarde essa informação.

O script não possui nenhuma dependência além do Python3 (nenhuma lib precisa ser instalada).
Para executá-lo, basta rodar:

$ python3 scripts/update_artistas.py
"""

import csv
import io
import json
from pathlib import Path
from urllib import request


STATIC_DIR = Path(__file__).parents[1].resolve() / 'public'
ARTISTS_JSON = STATIC_DIR / 'artists.json'
ARTISTS_IMAGES_DIR = STATIC_DIR / 'artistas'
ARTISTS_SPREADSHEET_URL = 'https://docs.google.com/spreadsheets/d/10Z76kagnJ-8Yg5OnsFlsNYuSHNvYO5X0-ufwZp4lxSc/export?format=csv'
CSV_HEADERS = ['Nome', 'URL img de perfil', 'Twitter', 'Github', 'Instagram', 'Website pessoal']
SLUG_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"


def get_artists_entries_from_spreadsheet():
    response = request.urlopen(ARTISTS_SPREADSHEET_URL)
    content = response.read().decode()
    csv_reader = csv.DictReader(io.StringIO(content), fieldnames=CSV_HEADERS)
    return [r for r in csv_reader][1:]


def format_spreadsheet_entry_for_json(row):
    return {
        "name": row["Nome"],
        "lower_name": row["Nome"].lower(),
        "image": row['URL img de perfil'],
        "links": {
            "website": row["Website pessoal"] or '',
            "twitter": row["Twitter"] or '',
            "github": row["Github"] or '',
            "instagram": row["Instagram"] or '',
        }
    }



if __name__ == '__main__':
    print("Lendo planilha dados da planilha do Google...")
    artists_entries = get_artists_entries_from_spreadsheet()
    new_data = [format_spreadsheet_entry_for_json(a) for a in artists_entries]
    new_data_by_name = dict((d["lower_name"], d) for d in new_data)

    print("Carregando JSON com dados dos artistas...")
    with open(str(ARTISTS_JSON)) as fd:
        artists_data = json.load(fd)

    print("Atualizando dados dos artistas ja presentes no JSON...")
    for artist in artists_data:
        update_with = new_data_by_name.pop(artist['lower_name'], None)
        if not update_with:
            continue
        for k, v in update_with.items():
            artist[k] = v

    print("Adicionando novos artistas ao JSON...")
    for new_artist in new_data_by_name.values():
        artists_data.append(new_artist)

    print("Atualizando o JSON final...")
    artists_data = sorted(artists_data, key=lambda a: a['lower_name'])
    with open(str(ARTISTS_JSON), 'w') as fd:
        json.dump(artists_data, fd, indent=2)
    print("%s atualizado com sucesso!" % ARTISTS_JSON)
