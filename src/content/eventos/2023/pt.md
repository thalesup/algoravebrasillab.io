---
title: Algorave Brasil 2023 (pt)
pubDate: 2023-11-25
description: Algorave 2023
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2023/pt">português</a></li>
  <li>español</li>
  <li>english</li>
</ul>
<p>Este é o site da edição anual da Algorave Brasil, um evento
Brasileiro organizado pela comunidade de mesmo nome, Algorave Brasil,
para performances ao vivo de live coding no dia 25 de Novembro de 2023
(sabado), de 9h a 00h, incluindo artistas visuais, artistas sonoros,
músicos, não-músicos, programadoras, mestres em gambiologia,
engenheiras, curiosos, iniciantes ou experientes.</p>

<p>O principal objetivo do evento é aproximar os interessados em <a
href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> no
Brasil.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/QhiMDZfL_nY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h1>Participe!</h1>
<p>(<em>inscrições encerradas</em>) <s>Para participar basta incluir o seu nome e os detalhes da sua performance no link abaixo.</s></p>
<ul>
  <li><a href="https://pad.riseup.net/p/AlgoraveBrasil2023-keep">https://pad.riseup.net/p/AlgoraveBrasil2023-keep</a></li>
</ul>

<h1 id="programacao">Programação</h1>

<table style="border-collapse: collapse">
<tr style="border: 1px dashed"><th>Hora</th><th>Nome, Cidade, Descrição da atividade</th></tr>
<tr style="border: 1px dashed"><td colSpan="2"><em>Nota: Horário de Brasília (UTC -3)</em></td></tr>
<tr style="border: 1px dashed"><td>- 09:00</td><td>Joenio M. Costa, Paris/FR, live-coding (sons e visuais) com dublang</td></tr>
<tr style="border: 1px dashed"><td>- 09:30</td><td>diegodukao, Rio de Janeiro, FoxDot fristaile</td></tr>
<tr style="border: 1px dashed"><td>- 10:00</td><td>Joenio + comunidade Algorave Brasil, Paris/FR, Boas-vindas e bate-papo aberto</td></tr>
<tr style="border: 1px dashed"><td>- 10:30</td><td>igor medeiros, recife/sao paulo, supercollider (talvez +tidalcycles)</td></tr>
<tr style="border: 1px dashed"><td>- 11:00</td><td>Mari Moura, Paris, Live Crochet Coding (Sonic Pi + Crochet)</td></tr>
<tr style="border: 1px dashed"><td>- 11:30</td><td>Bruxaria Digital, Natal/RN, performance digital com live coding (Hydra + Sonic Pi)</td></tr>
<tr style="border: 1px dashed"><td>- 12:00</td><td>nomabits, Paris/FR, live-coding dublang + (Sonic Pi, Tidal Cycles, etc)</td></tr>
<tr style="border: 1px dashed"><td>- 12:30</td><td>Joenio, Paris/FR, Talk: Introducao ao sistema dublang</td></tr>
<tr style="border: 1px dashed"><td>- 13:00</td><td>violeta, Barcelona, improviso de música experimental com FoxDot</td></tr>
<tr style="border: 1px dashed"><td>- 13:30</td><td>Mari, relato de experiência: 1 ano como live coders /Iclc/Tour 2023</td></tr>
<tr style="border: 1px dashed"><td>- 14:00</td><td>-livre-</td></tr>
<tr style="border: 1px dashed"><td>- 14:30</td><td>-livre-</td></tr>
<tr style="border: 1px dashed"><td>- 15:00</td><td>batata, Monteiro Lobato, live-coding foxdot</td></tr>
<tr style="border: 1px dashed"><td>- 15:30</td><td>Jackson Marinho, Brasília, Tropical Pad</td></tr>
<tr style="border: 1px dashed"><td>- 16:00</td><td>Ángel Jara, Buenos Aires, live-coding Sonic-pi</td></tr>
<tr style="border: 1px dashed"><td>- 16:30</td><td>Rádio Toba + a1219, São Paulo, tidal + Hydra</td></tr>
<tr style="border: 1px dashed"><td>- 17:00</td><td>Fuser, São Paulo, tidal e SuperCollider</td></tr>
<tr style="border: 1px dashed"><td>- 17:30</td><td>Sabonete Cirúrgico, São Paulo, tidal </td></tr>
<tr style="border: 1px dashed"><td>- 18:00</td><td>jam (a1219, Rádio Toba e Fuser), São Paulo, tidal, SuperCollider e Hydra no flok</td></tr>
<tr style="border: 1px dashed"><td>- 18:30</td><td>-livre-</td></tr>
<tr style="border: 1px dashed"><td>- 19:00</td><td>namdlogg, Rio de Janeiro/RJ, Set</td></tr>
<tr style="border: 1px dashed"><td>- 19:30</td><td>-livre-</td></tr>
<tr style="border: 1px dashed"><td>- 20:00</td><td>BSBLOrk, Brasilia/DF, EPA#3 Experiência Peripatética Abstrata: Sobre Live Coding</td></tr>
<tr style="border: 1px dashed"><td>- 20:30</td><td>ali, Brumadinho/MG, Experimento LC + eletrônica</td></tr>
<tr style="border: 1px dashed"><td>- 21:00</td><td>euFraktus_X, Brasília/DF, EPA#23 Experiência Peripatética Abstrata: Sobre Live Coding</td></tr>
<tr style="border: 1px dashed"><td>- 21:30</td><td>lixt, Pietrasanta/IT, Ambient Drones com Threnoscope + Hydra</td></tr>
<tr style="border: 1px dashed"><td>- 22:00</td><td>Dunossauro, Botucatu/SP, foxdot lofi</td></tr>
<tr style="border: 1px dashed"><td>- 22:30</td><td>Bruno Gola, berlim, ? TBC</td></tr>
<tr style="border: 1px dashed"><td>- 23:00</td><td>fracto & Sasha, Recife/PE,manipulação de imagem com Hydra durante perfomance visual de Sasha</td></tr>
<tr style="border: 1px dashed"><td>- 23:30</td><td>Leacolumbi, Ibagué COL, experimento n con Eli e-guitarra</td></tr>
</table>

<p><img src="/algorave-brasil-2023____________________@bruxariadigital___.jpg" /></p>

<h1 id="comunidades">Comunidades locais organizadas</h1>

<table>
<tr><th>Cidade</th><th>Local, Horario, Organizadores</th></tr>
<tr><td>São Paulo</td><td>Roda Livecodera Mafagafa, 16h30 - 18h30, Gil Fuser</td></tr>
</table>

<h1>Dúvidas?</h1>
<p>Fale conosco através do nosso <a href='tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'>grupo no telegram</a>. Se você quer participar mas não sabe como, se precisa de uma mãozinha ou tem qualquer outra dúvida sobre o evento, fique à vontade para nos procurar pelo telegram, ele tem sido o meio "oficial" de comunicação com a comunidade Algorave Brasil.</p>
<h1>Agradecimentos</h1>
<p>A toda a comunidade Algorave Brasil.</p>
</div>
