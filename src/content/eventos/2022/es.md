---
title: Algorave Brasil 2022 (es)
pubDate: 2022-11-26
description: Algorave 2022
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2022/pt">português</a></li>
  <li><a href="/eventos/2022/es">español</a></li>
  <li><a href="/eventos/2022/en">english</a></li>
</ul>
<p>Este es el sitio web de la edición anual de Algorave Brasil, un evento brasileño organizado por la comunidad del mismo nombre, Algorave Brasil, para presentaciones live coding en vivo los días 12 y 13 de diciembre de 2022, de 14:00 a 18:00, incluyendo artistas visuales, artistas de sonido, músicxs, no músicxs, programadores, maestrxs en gambiología, ingenierxs, curiosxs, principiantes o experimentadxs.</p>
<p>El principal objetivo del evento es reunir interesadxs en <a href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> en Brasil.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/jPU2nTxoyJk?start=1710" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
&nbsp;
<iframe width="560" height="315" src="https://www.youtube.com/embed/tMRAJQ0LWAA?start=1444" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h1>Participar!</h1>
<p>(<em>registro cerrado</em>) <s>Para participar, solo incluya su nombre y detalles de su desempeño en la hoja de cálculo a continuación.</s></p>
<ul>
  <li><a href="https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0">https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0</a></li>
</ul>

<h2>Instrucciones de transmisión</h2>
<ul>
  <li><a href="/2022-transmissao/es">Consulte aquí las instrucciones sobre cómo transmitir su interpretación</a>.</li>
</ul>
<h1>¡Síguelo en vivo!</h1>
<p>
  <ul>
    <li><a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a></li>
    <li><a href="https://www.twitch.tv/algoravebr">Twitch</a></li>
    <li>rtmp://biniou.net/algorave/algoravebrasil</li>
  </ul>
</p>
<h1>Programación</h1>
<iframe width="100%" height="650px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT05KeqHcZ-VRtK6nmakNdyJa3lJpCbljzta8Q_K5VwB-YvMKinWri_K62F9mTngvX3RAYvIfmxhW-m/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
<h1>¿Dudas?</h1>
<p>Habla con nosotros a través de nuestro <a href='tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'>grupo en Telegram</a>.Si quieres participar pero no sabes cómo, si necesitas una mano o tienes alguna otra pregunta sobre el evento, no dudes en contactarnos por Telegram, ha sido elegido como el medio de comunicación "oficial" por la comunidad de Algorave Brasil. </p>
<h1>Agradecimientos</h1>
<p>A toda la comunidad de Algorave Brasil, especialmente a lxs artistas participantes: djalgoritmo, irisS, n0p | flavio, Indizível, Pietro Bapthysthe, botella, Lowbin, Alexandre Rangel, ghales, Gil Fuser, Bedran & Kastrup, igor & vito, berin | vinícius, beise, euFraktus X, Hernani Villaseñor, BSBLOrk, fmiramar, diegodukao y los organizadores: Igor, ghales, Gil Fuser, berin, vinícius, beise y diegodukao. (Haznos saber si olvidamos agregar tu nombre aquí).</p>
<p>
  Un agradecimiento especial también a Olivier "oliv3" Girondel, desarrollador del software libre <a href="https://biniou.net">Le Biniou</a>, por ofrecer el servidor RTMP utilizado para transmitir este evento.</p>
</div>
