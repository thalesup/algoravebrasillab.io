---
title: Algorave Brasil 2020 (en)
pubDate: 2020-12-12
description: Algorave 2020
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2020/pt">português</a></li>
  <li><a href="/eventos/2020/es">español</a></li>
  <li><a href="/eventos/2020/en">english</a></li>
</ul>
<p>This is the page for the annual edition of Algorave Brasil, a Brazilian event organized by our community for online live-coding performances on December the 12th and 13th, 2020, from 2PM to 6PM (Sao Paulo local time). The event includes visual and sound artists, musicians, non-musicians, programmers, work-around masters, engineers, curious people, beginners or experienced programmers alike.</p>
<p>The purpose of the event is to gather those interested in <a href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> in Brazil.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/jPU2nTxoyJk?start=1710" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
&nbsp;
<iframe width="560" height="315" src="https://www.youtube.com/embed/tMRAJQ0LWAA?start=1444" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h1>Join us!</h1>
<p>(<em>closed for now</em>) <s>To participate in the event, just include your name and perfomance details on the spreadsheet below</s></p>
<ul>
  <li><a href="https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0">https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0</a></li>
</ul>

<h2>Streaming Instructions</h2>
<ul>
  <li><a href="/eventos/2020-transmissao/en">Check out our instructions on how to stream your performance</a>.</li>
</ul>
<h1>Watch it Live!</h1>
<p>
  <ul>
    <li><a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a></li>
    <li><a href="https://www.twitch.tv/algoravebr">Twitch</a></li>
    <li>rtmp://biniou.net/algorave/algoravebrasil</li>
  </ul>
</p>
<h1>Event Schedule</h1>
<iframe width="100%" height="650px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT05KeqHcZ-VRtK6nmakNdyJa3lJpCbljzta8Q_K5VwB-YvMKinWri_K62F9mTngvX3RAYvIfmxhW-m/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
<h1>Questions?</h1>
<p>Reach us through our <a href='tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'>Telegram group</a>. If you want to participate but don't know how, if you need a hand or has any other questions about the event, feel free to reach us through telegram - it's been our "very official" means of communication for now</p>
<h1>Thanks</h1>
<p>To the entire Algorave Brasil community, in particular to the participating artists: djalgoritmo, irisS, n0p | flavio, Indizível, Pietro Bapthysthe, garrafada, Lowbin, Alexandre Rangel, ghales, Gil Fuser, Bedran & Kastrup, igor & vito, berin | vinícius, beise, euFraktus X, Hernani Villaseñor, BSBLOrk, fmiramar, diegodukao and our organizers: Igor, ghales, Gil Fuser, berin, vinícius, beise djalgoritmo, and diegodukao. (Did we forget you? Message us!).</p>
<p>Special thanks to Olivier "oliv3" Girondel, developer of the free software project <a href="https://biniou.net">Le Biniou</a>, for providing the RTMP server used for this stream</p>
</div>
