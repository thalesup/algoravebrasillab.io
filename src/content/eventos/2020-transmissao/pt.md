---
title: Instruções para transmissão (Streaming) 2020
pubDate: 2020-12-12
description: Instruções para transmissão (Streaming)
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2020-transmissao/pt">português</a></li>
  <li><a href="/eventos/2020-transmissao/es">español</a></li>
  <li><a href="/eventos/2020-transmissao/en">english</a></li>
</ul>
<ul>
  <li>Os slots são de 30 minutos (no máximo), mas você pode alocar vários slots se preferir.</li>
  <li>Cada performance deve começar e terminar no horário.</li>
  <li>Inicie a transmissão no horário correto. Se você entrar antes pode cortar a transmissão anterior.</li>
  <li>Se possível adicione informações no vídeo incluindo seu nome, local, etc.</li>
  <li>Recomendamos o uso do Open Broadcaster Software (OBS), software livre disponível para Linux, Mac e Windows em <a href="https://obsproject.com">https://obsproject.com</a>.</li>
  <li>Você pode encontrar a organização e artistas antes, durante e após o evento para uma cerveja &#127866; e dúvidas em geral no <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
</ul>

<h4 id="configuração">Configuração do OBS</h4>
<p>Abra as configurações do OBS em “File/Arquivo” &gt; “Settings/Configurações”. Lá acesse as seguintes abas e defina os parâmetros da transmissão:</p>

<ul>
  <li>Na aba “Stream/Transmissão”:
<br />
    <img src="/obs-stream.png" />
    <ul>
      <li>Service/Serviço = Custom.../Personalizado...</li>
      <li>Server/Servidor = rtmp://biniou.net/algorave</li>
      <li>Stream Key/Chave da transmissão = <em>COPIE E COLE O TOKEN RECEBIDO POR EMAIL OU TELEGRAM</em></li>
    </ul>
  </li>
  <li>Na aba "Output/Saída" &gt; "Streaming/Transmissão":
<br />
    <img src="/obs-output.png" />
    <ul>
      <li>Video bitrate/Taxa de bits do vídeo = 1000 Kbps
<ul>
          <li>Dica! <em>1000 Kbps de bitrate é um valor razoável e garante uma
          qualidade média de vídeo, se notar gargalos tente reduzir para 500,
          ou se necessitar de mais qualidade tente valores em torno de 2000,
          requer uma conexão de internet boa mas garante melhor qualidade
  de vídeo.</em></li>
        </ul>
      </li>
      <li>Audio bitrate/Taxa de bits do áudio = 128</li>
    </ul>
  </li>
  <li>Na aba “Video”:
<br />
    <img src="/obs-video.png" />
    <ul>
      <li>Output (Scaled) resolution/Resolução de saída (escala) = 1280x720.</li>
      <li>Common FPS values/Valor de FPS comum = 20
<ul>
          <li>Dica! <em>Se perceber gargalos tente 10 FPS, ou se tiver uma
          boa conexão de internet seja boa suficiente tente valores
  maiores como 30 FPS por exemplo.</em></li>
        </ul>
      </li>
    </ul>
  </li>
</ul>

<p>Estes valores de configuração definem uma transmissão de qualidade média e
segura para evitar problemas de conexão, caso você tenha uma boa conexão pode
usar valores de bitrate, fps ou resolução maiores.  Estas são apenas sugestões
gerais, você pode preferir definir seus próprios parâmetros.</p>

<h4 id="teste-sua-transmissão">Teste sua transmissão (streaming de testes)</h4>
<ol>
  <li>Os testes devem ser feito antes do início do evento.
    <ul>
      <li>O stream de testes <a href="https://youtu.be/lZxo0ftXxkA">Algorave Brasil 2020 (test stream)</a> estará disponível apenas até 13:00 do dia 12 (sábado).</li>
      <li>É possível fazer testes após isso usando outros canais (YouTube, Twitch.tv, etc), se precisar de ajuda pergunte no <a href="tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA">Telegram</a> ou no <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
    </ul>
  </li>
  <li>Inicie a transmissão de teste em “Start Streaming/Iniciar transmissão”
<br />
    <img src="/obs-startstream.png" />
  </li>
  <li>Verifique se está ao vivo no canal "Algorave Brasil 2020 (test stream)" em:
<ul>
      <li><a href="https://youtu.be/lZxo0ftXxkA">https://youtu.be/lZxo0ftXxkA</a></li>
    </ul>
  </li>
  <li>Para encerrar a transmissão de testes clique em "Stop streaming/Interromper transmissão".
  <br />
    <img src="/obs-stopstream.png" />
  </li>
</ol>

<p>Uma boa fonte para ajuda caso algo dê errado no teste é essa aqui:</p>
<ul>
  <li><a href="https://support.google.com/youtube/answer/2853702?hl=en-GB">https://support.google.com/youtube/answer/2853702?hl=en-GB</a></li>
</ul>

<h4 id="na-hora-da-performance">Na hora da performance (streaming oficial)</h4>
<ol>
  <li>Aguarde até o momento da sua hora agendada na <a href="https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0">planilha de performances do evento</a>.</li>
  <li>Inicie sua transmissão em “Start Streaming/Iniciar transmissão”
<br />
    <img src="/obs-startstream.png" />
  </li>
  <li>A transmissão estará ao vivo no <a
    href="https://www.twitch.tv/algoravebrasil">canal da Algorave Brasil no
Twitch</a> e também no <a
      href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a>,
além de estar também disponível via RTMP em:
<ul><li>rtmp://biniou.net/algorave/algoravebrasil</li></ul>
  </li>
  <li>Prepare-se para terminar a transmissão alguns segundos antes do fim do seu horário.
<br />
    <img src="/obs-stopstream.png" />
  </li>
  <li>É isso, agradecemos a sua participação e esperamos você no &#127866; <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
</ol>

<p><small>
  <h3>Créditos:</h3>
  <ul>
    <li>
      O conteúdo dessa página foi inspirado nas instruções do Eulerroom Equinox,
      disponível em
<a href="https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew">
        https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew
</a>
    </li>
    <li>
      O servidor RTMP utilizado para transmissão da Algorave Brasil 2020 foi
gentilmente cedido pelo projeto de software livre <a href="https://biniou.net">Le Biniou</a>
    </li>
  </ul>
</small>
</p>
</div>
