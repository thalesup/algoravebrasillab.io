---
title: Algorave Brasil 2021 (pt)
pubDate: 2021-12-11
description: Algorave 2021
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2021/pt">português</a></li>
  <li>español</li>
  <li><a href="/eventos/2021/en">english</a></li>
</ul>
<p>Este é o site da edição anual da Algorave Brasil, um evento Brasileiro organizado pela comunidade de mesmo nome, Algorave Brasil, para performances ao vivo de live coding nos dias 11 e 12 de Dezembro de 2021, das 14h as 18h, incluindo artistas visuais, artistas sonoros, músicos, não-músicos, programadoras, mestres em gambiologia, engenheiras, curiosos, iniciantes ou experientes.</p>
<p>O principal objetivo do evento é aproximar os interessados em <a href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> no Brasil.</p>

<p><a href="https://www.youtube.com/watch?v=uP_a_kXsCto" target="_blank">Transmissão do dia 11 (Sábado)</a></p>
<p><a href="https://www.youtube.com/watch?v=8TrtuSZpx10" target="_blank">Transmissão do dia 12 (Domingo)</a></p>

<h1>Participe!</h1>
<p>(<em>inscrições encerradas</em>) <s>Para participar basta incluir o seu nome e os detalhes da sua performance na planilha abaixo.</s></p>

<h2>Instruções de transmissão (streaming)</h2>
<ul>
    <li><a href="/2021-transmissao/pt">Veja aqui as instruções de como transmitir a sua performance</a>.</li>
</ul>
<h1>Acompanhe ao vivo!</h1>
<p>
  <ul>
    <li><a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a></li>
  </ul>
</p>
<h1>Programação</h1>
  <iframe width="100%" height="650px" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQvuHu9BU328OAJuDPWYPRf7XT2MwQmG2YiW0eV-c6vzmR227U5caTJ74pHZK6lmER_iImolGiZ08or/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
<h1>Dúvidas?</h1>
<p>Fale conosco através do nosso <a href='tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'>grupo no telegram</a>. Se você quer participar mas não sabe como, se precisa de uma mãozinha ou tem qualquer outra dúvida sobre o evento, fique à vontade para nos procurar pelo telegram, ele tem sido o meio "oficial" de comunicação com a comunidade Algorave Brasil.</p>
<h1>Agradecimentos</h1>
<p>A toda comunidade Algorave Brasil,
em especial aos artistas participantes:
Pietro Bapthysthe,
fmiramar,
Bruno Gola,
diegodorado,
alovictor,
smosgasbord,
lowbin,
Rafrobeat,
euFraktus_X,
Santiago Ramírez Camarena,
diegodukao,
beise,
Indizível (brunorohde),
Gil Fuser,
Mega Jam FoxDot,
Ángel Jara,
igor | vito,
BSBLOrk
</p>
</div>
