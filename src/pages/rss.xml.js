import rss from '@astrojs/rss';
import { getCollection } from 'astro:content';
import { SITE_TITLE, SITE_DESCRIPTION } from '../consts';

export async function get(context) {
  const eventos = await getCollection('eventos');
  return rss({
    title: SITE_TITLE,
    description: SITE_DESCRIPTION,
    site: context.site,
    items: eventos.map((evento) => ({
      ...evento.data,
      link: `/eventos/${evento.slug}/`,
    })),
  });
}
