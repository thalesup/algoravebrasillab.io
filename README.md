## Algorave Brasil

[Página](http://algoravebrasil.ml) da comunidade brasileira de live-coding e algorave.
🚧 Em construção! 🚧

## 🙋 Quero aparecer na listagem de artistas!

Para adicionar suas informações na nossa [página de artistas](http://algoravebrasil.ml/artistas), primeiro você precisa colocar as suas informações (nome, links etc) [nesta planilha do Google Drive](https://docs.google.com/spreadsheets/d/10Z76kagnJ-8Yg5OnsFlsNYuSHNvYO5X0-ufwZp4lxSc/edit#gid=0). Depois disso, existe duas maneiras de atualizar o site:

1. A maneira **simples**: caso você ainda não faça parte do nosso grupo no Telegram, [clique aqui para entrar](tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA). Lá no grupo sinta-se a vontade para pedir para alguém atualizar o site para você. Não pense que você está incomodando pois o procesos é automatizado e simples e, além de tudo, você é muito bem-vinda;
2. A maneira **trabalhosa**: ter o projeto rodando na sua máquina e atuaizar o JSON com os dados dos artistas. Para isso, basta seguir os passos descritos na sessão [como contribuir](https://gitlab.com/algoravebrasil/algoravebrasil.gitlab.io/-/blob/master/README.md#-como-contribuir) e também ter o [Python3](https://www.python.org/) instalado na sua máquina. Depois disso, rode o seguind comando a partir da raiz do repositório: `python3 scripts/update_artistas.py` e abra um PR com suas alterações;

**IMPORTANTE**: Ao atualizar os dados na planilha, tenha cuidado para não alterar dados das outras pessoas. A planilha é pública e *grandes poderes, grandes reponsabilidades*.

## ✨ Como Contribuir

0. Não sabe onde ajudar? Veja nossas [issues](https://gitlab.com/algoravebrasil/algoravebrasil.gitlab.io/-/boards) :)

1. Tire um [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) deste repositório
  - Instale o `npm` e execute `npm install`
2. Faça suas alterações.
  - `npm run dev` OU `docker-compose up`
  - Site disponível em `localhost:3000`
3. Garanta que o site consegue compilar: rode os comandos abaixo, e veja se algum erro aparece
  - `npm run build`
  - `npm start`
  - Site disponível em `localhost:3000`
4. Abra um [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests)
5. Se necessário, avise no grupo (telegram) o que você fez: o link para entrar fica [nessa página] (http://algoravebrasil.ml/entre)

Lembrando: em caso de dúvida, fale conosco no grupo!

### 🛑 Cuidados:

1. Não adicione arquivos à pasta `public`: isso impede o site de compilar pelo gitlab pages. A pasta `public` é um diretório reservado tanto para o `next.js` quanto para o `gitlab pages`
